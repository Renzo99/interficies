package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;



public class Controller {



     @FXML
    TextArea Texto;

    @FXML
    MenuItem salir;

    @FXML
    MenuItem Copiar;

    @FXML
    MenuItem Tallar;

    @FXML
    MenuItem Enganxar;

    @FXML
    MenuItem Desfer;

    @FXML
    Menu Editar;

    @FXML
    MenuItem NotoSans;

    @FXML
    MenuItem Negrita;

    @FXML
    MenuItem Hack;



    @FXML
    MenuItem x16;

    @FXML
    MenuItem x18;

    @FXML
    MenuItem x22;


    public void saliendo(){
        Platform.exit();
    }

    public void copiando(){
       // textoSelecionado = Texto.getSelectedText();
        Texto.copy();
    }

    public void pegando(){
       // Texto.insertText(Texto.getCaretPosition(),textoSelecionado);
        Texto.paste();
    }

    public void cortando(){
        Texto.cut();
    }

    public void desfer(){
        Texto.undo();
    }
    public void clickEditar(){
        if (Texto.getSelectedText().length() ==0) {
            Copiar.setDisable(true);
            Tallar.setDisable(true);
        }
        else {
            Copiar.setDisable(false);
            Tallar.setDisable(false);
        }
    }

    public void toNotoSans() {

        Texto.setStyle("-fx-font-family: 'Noto Sans'");

    }

    public void toHack() {
        Texto.setStyle("-fx-font-family: 'Hack'");
    }

    public void toNegrita() {
        Texto.setStyle("-fx-font-family: 'Lato Black'");
    }


    public void setSize(ActionEvent actionEvent) {
         int valor = Integer.parseInt(((MenuItem) actionEvent.getSource()).getText());

         Texto.setStyle("-fx-font-size: "+valor);
    }
}
